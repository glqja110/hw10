using namespace std;
#include "aus_vote.h"

/*
vector<string> Candidates;
vector<int> Preferences;
typedef vector<Candidate> RoundResult; 

struct Candidate { 
 string name; 
 int votes; 
}; 
*/ 

AusVoteSystem::AusVoteSystem(const vector<string>& candidate_names) { //후보자 이름 등록
  for(int i=0;i<candidate_names.size();++i) {
    Candidates.push_back(candidate_names[i]);
  }
}

bool AusVoteSystem::AddVote(const vector<int>& vote, int voter) {
  if(vote.size()/voter!=Candidates.size()) return false;  //선호도 입력한게 후보자 수랑 다르게 나오면
  else {
    for(int i=0;i<Candidates.size();++i) {
      for(int j=0;j<voter;++j) {
        Preferences.push_back(vote[i+Candidates.size()*j]);
      }
    }
    voter_=voter;
    return true;
  }
}

void AusVoteSystem::CountResult() {       
  RoundResult.clear();
  int count=0;
  for(int i=0; i<Candidates.size();++i) {
    for(int j=0;j<voter_;++j) {
        if(Preferences[j]==i+1) {      //투표한 번호가 해당 사람이랑 일치하면 센다 
        count++;
      }
    }
    Candidate can={Candidates[i],count};
    RoundResult.push_back(can);
    count=0;
    //cout<<"여기까진 됨"<<endl;
  }
}

void AusVoteSystem::YouLose(int select) {               //진 사람 고른 사람들 순위 변동하는 함수
  for(int i=0;i<Candidates.size()-1;++i) {
    int tmps;
    tmps=Preferences[voter_*i+select];
    Preferences[voter_*i+select]=Preferences[voter_*(i+1)+select];
    Preferences[voter_*(i+1)+select]=tmps;
  }
}

