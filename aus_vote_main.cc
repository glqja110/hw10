using namespace std;
#include "aus_vote.h"

/*
vector<string> Candidates;
vector<int> Preferences;
typedef vector<Candidate> RoundResult; 

struct Candidate { 
 string name; 
 int votes;
 string Name_() {return name;}
 int Votes_() {return votes;}
}; 
*/ 

int main() {
  vector<string> Names;
  vector<int> Preference;
  int can_num;
  int peo_num; 
  int situation=0;
  int checking=0;
  int LoserIs=0;
  int chooseLoser=0;
  int LoserHere=0;
  int LeastPicked;
  int checkdouble=0;
  vector<int> Except;

  cin>>can_num;
  for(int i=0;i<can_num;++i) {
    string c_name;
    cin>>c_name;
    Names.push_back(c_name);
  }
  AusVoteSystem Aus(Names);  //class 선언
  
  cin>>peo_num;   //투표하는 사람들 수
  for(int i=0;i<peo_num*can_num;++i) {
    int pre_num;
    cin>>pre_num;
    Preference.push_back(pre_num);
  }
  if(!Aus.AddVote(Preference,peo_num)) exit(0);
  int round=1;
  vector<string> Winners;
  //vector<int> Lose_Pick;   //진사람들 1순위로 놓은 사람들 밀어내기

  while(true) {
    Aus.CountResult();         //1순위에 있는것들 기반으로 점수 매기기
    cout<<"Round "<<round<<": ";
    for(int i=0;i<(Aus.ComputeResult()).size();++i) {       //형식에 맞게 출력
      for(int j=0;j<Losers.size();++j) {
        if(Losers[j]==i+1) LoserIs=1;
      }
      if(LoserIs==0) cout<<(Aus.ComputeResult())[i].Name_()<<' '<<(Aus.ComputeResult())[i].Votes_()<<' ';
      LoserIs=0;
      //cout<<"여기까진 됨"<<endl;
    }
    cout<<endl;

     if(Losers.size()==can_num-1) {             //이긴 사람이 하나뿐이면
      situation=1;
      break;
    }

 
    //여기부터 꼴찌 골라내는 작업 할거임
    int nolose=0;
    //cout<<"여기 까진 됨?"<<endl;

    for(int i=0;i<(Aus.ComputeResult()).size();++i) {
      for(int j=0;j<Losers.size();++j) {
        if(Losers[j]==i+1) {
          //cout<<"웬 패배자가?"<<endl;
          nolose=1; 
        }
      }
      if(nolose==0) {
        Except.push_back(i+1);          //제외 하지 않는 명단 
        //cout<<"넣었어요! ^0^"<<endl;
      }
      nolose=0; 
    }
    /*for(int i=0;i<Except.size();++i) {
      cout<<"제외자명단!!: "<<Except[i]<<endl;
    }*/
       
    //cout<<"여기 까진 됨?"<<endl;


    /*LeastPicked=(Aus.ComputeResult())[Except[0]-1].Votes_();
    //cout<<"미떼! :"<<LeastPicked<<endl;
    for(int i=0;i<Except.size()-1;++i) {
      if(LeastPicked>(Aus.ComputeResult())[Except[i+1]-1].Votes_());
          LeastPicked=(Aus.ComputeResult())[Except[i+1]-1].Votes_();
    }*/

    //cout<<"가장 적은 표!: "<<LeastPicked<<endl;

   int tmp;
   for(int i=0;i<Except.size()-1;++i) {
       for(int j=0;j<Except.size()-1;++j) {
	  if((Aus.ComputeResult())[Except[j]-1].Votes_()<(Aus.ComputeResult())[Except[i+1]-1].Votes_()) {
	    tmp=Except[j];
	    Except[j]=Except[j+1];
	    Except[j+1]=tmp;
	  }
       }
   }
   int different=0;
   for(int i=0;i<Except.size()-1;i++) {
     if((Aus.ComputeResult())[Except[i]-1].Votes_()!=(Aus.ComputeResult())[Except[i+1]-1].Votes_()) different=1;
   }

   if(different==0) {     //표가 다 똑같으면
      situation=2;
      break;  
   }

   
   for(int i=0;i<Except.size()-1;++i) {
     if( (Aus.ComputeResult())[Except[i]-1].Votes_() != (Aus.ComputeResult())[Except[Except.size()-1]].Votes_() )
       Winners.push_back( (Aus.ComputeResult())[Except[i]-1].Name_());
   }
   Losers.push_back(Except[Except.size()-1]);              //끝에 있는 애 (표 가장 적은 놈) 를 Loser로 놓는다.
   
   for(int i=0;i<Except.size()-1;++i) {
     if((Aus.ComputeResult())[Except[i]-1].Votes_()==(Aus.ComputeResult())[Except[Except.size()-1]-1].Votes_()) {
       checkdouble=1;
       Losers.push_back(Except[i]);
     }
   }
   

    if(Winners.size()==1&&Losers.size()==can_num-1&&checkdouble==0) {             //이긴 사람이 하나뿐이면
      situation=1;
      break;
    }

    else {                //아직 Winner를 고르지 못했으면
      Winners.clear();
      for(int i=0;i<peo_num;++i) {              //투표한 사람들을 확인해서
        for(int j=0;j<Losers.size();++j) {       
          if(Losers[j]==(Aus.Voters())[i]) chooseLoser=1;      //투표한 번호가 진사람들 중에 있었으면
        }
        if(chooseLoser==1) {
          Aus.YouLose(i);
          --i;
        }
        chooseLoser=0;
      }
      
      ++round;
    }
    different=0;
    checkdouble=0;
    Except.clear();
 }   

 if(situation==1) 
   cout<<"Winner: "<<Winners[0]<<endl;
 else if(situation==2) 
   cout<<"Winner: "<<endl;

  return 0;
}
