#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include <stdio.h>      
#include <stdlib.h>    

using namespace std;

int main(int argc, char** argv) {
  vector<string> Result1;
  vector<string> Result2;
  vector<string> Result3;
  vector<string> Result4;
  vector<string> Result5;

  vector<string> Out1;
  vector<string> Out2;
  vector<string> Out3;
  vector<string> Out4;
  vector<string> Out5;

  string filename1;
  string filename2;
  string filename3;
  string filename4;
  string filename5;

  if((argv[1])[0] == 't') {
    filename1="torrent_data/1.out";
    filename2="torrent_data/2.out";
    filename3="torrent_data/3.out";
    filename4="torrent_data/4.out";
    filename5="torrent_data/5.out";
  } else if((argv[1])[0] == 'a') {
    filename1="aus_vote_data/1.out";
    filename2="aus_vote_data/2.out";
    filename3="aus_vote_data/3.out";
    filename4="aus_vote_data/4.out";
    filename5="aus_vote_data/5.out";
  } else if((argv[1])[0] == 'p') {
    filename1="poly_diff_data/1.out";
    filename2="poly_diff_data/2.out";
    filename3="poly_diff_data/3.out";
    filename4="poly_diff_data/4.out";
    filename5="poly_diff_data/5.out";
  } else if((argv[1])[0] == 's') {
    filename1="sorted_array_data/1.out";
    filename2="sorted_array_data/2.out";
    filename3="sorted_array_data/3.out";
    filename4="sorted_array_data/4.out";
    filename5="sorted_array_data/5.out";
  } else if((argv[1])[0] == 'd') {
    filename1="draw_shape_data/1.out";
    filename2="draw_shape_data/2.out";
    filename3="draw_shape_data/3.out";
    filename4="draw_shape_data/4.out";
    filename5="draw_shape_data/5.out";
  }

  char filen1[filename1.length()];
  for(int i=0;i<filename1.length();++i) {
    filen1[i]=filename1[i];
  }
  
  char filen2[filename2.length()];
  for(int i=0;i<filename2.length();++i) {
    filen2[i]=filename2[i];
  }
  char filen3[filename3.length()];
  for(int i=0;i<filename3.length();++i) {
    filen3[i]=filename3[i];
  }
  char filen4[filename4.length()];
  for(int i=0;i<filename4.length();++i) {
    filen4[i]=filename4[i];
  }
  char filen5[filename5.length()];
  for(int i=0;i<filename5.length();++i) {
    filen5[i]=filename5[i];
  }
  if((argv[1])[0] == 't') {
    system("./torrent <torrent_data/1.in> result1.txt");
    system("./torrent <torrent_data/2.in> result2.txt");
    system("./torrent <torrent_data/3.in> result3.txt");
    system("./torrent <torrent_data/4.in> result4.txt");
    system("./torrent <torrent_data/5.in> result5.txt");
  } else if((argv[1])[0] == 'a') {
    system("./aus_vote <aus_vote_data/1.in> result1.txt");
    system("./aus_vote <aus_vote_data/2.in> result2.txt");
    system("./aus_vote <aus_vote_data/3.in> result3.txt");
    system("./aus_vote <aus_vote_data/4.in> result4.txt");
    system("./aus_vote <aus_vote_data/5.in> result5.txt");
  } else if((argv[1])[0] == 'p') {
    system("./poly_diff <poly_diff_data/1.in> result1.txt");
    system("./poly_diff <poly_diff_data/2.in> result2.txt");
    system("./poly_diff <poly_diff_data/3.in> result3.txt");
    system("./poly_diff <poly_diff_data/4.in> result4.txt");
    system("./poly_diff <poly_diff_data/5.in> result5.txt");
  } else if((argv[1])[0] == 's') {
    system("./sorted_array <sorted_array_data/1.in> result1.txt");
    system("./sorted_array <sorted_array_data/2.in> result2.txt");
    system("./sorted_array <sorted_array_data/3.in> result3.txt");
    system("./sorted_array <sorted_array_data/4.in> result4.txt");
    system("./sorted_array <sorted_array_data/5.in> result5.txt");
  } else if((argv[1])[0] == 'd') {
    system("./draw_shape <draw_shape_data/1.in> result1.txt");
    system("./draw_shape <draw_shape_data/2.in> result2.txt");
    system("./draw_shape <draw_shape_data/3.in> result3.txt");
    system("./draw_shape <draw_shape_data/4.in> result4.txt");
    system("./draw_shape <draw_shape_data/5.in> result5.txt");
  }


//예제 파일 입력하고 그 결과를 result(number).txt 에 넣는 과정              



  //결과 파일 스트링으로 처리해서 넣기

  ifstream infile1;
  infile1.open ("result1.txt");
  string sLine1 = "";
  while (!infile1.eof())
  {
    getline(infile1, sLine1);
    if(sLine1!="") {
      if(sLine1[sLine1.length()-1]==' ') sLine1.erase(sLine1.length()-1);
      Result1.push_back(sLine1);
    }
  }
  infile1.close();

  ifstream infile2;
  infile2.open ("result2.txt");
  string sLine2 = "";
  while (!infile2.eof())
  {
    getline(infile2, sLine2);
    if(sLine2!="") {
      if(sLine2[sLine2.length()-1]==' ') sLine2.erase(sLine2.length()-1);
      Result2.push_back(sLine2);
    }
  }
  infile2.close();

  ifstream infile3;
  infile3.open ("result3.txt");
  string sLine3 = "";
  while (!infile3.eof())
  {
    getline(infile3, sLine3);
    if(sLine3!="") {
      if(sLine3[sLine3.length()-1]==' ') sLine3.erase(sLine3.length()-1);
      Result3.push_back(sLine3);
    }
  }
  infile3.close();

  ifstream infile4;
  infile4.open ("result4.txt");
  string sLine4 = "";
  while (!infile4.eof())
  {
    getline(infile4, sLine4);
    if(sLine4!="") {
      if(sLine4[sLine4.length()-1]==' ') sLine4.erase(sLine4.length()-1);
      Result4.push_back(sLine4);
    }
  }
  infile4.close();

  ifstream infile5;
  infile5.open ("result5.txt");
  string sLine5 = "";
  while (!infile5.eof())
  {
    getline(infile5, sLine5);
    if(sLine5!="") {
      if(sLine5[sLine5.length()-1]==' ') sLine5.erase(sLine5.length()-1);
      Result5.push_back(sLine5);
    }
  }
  infile5.close();


//출력 파일 들을 벡터에 저장
  
  char outname1[sizeof(filen1)];
  for(int i=0;i<sizeof(filen1);++i) {
    outname1[i]=filen1[i];
  }
  ifstream infile6;
  infile6.open (outname1);
  string sLine6 = "";
  while (!infile6.eof())
  {
    getline(infile6, sLine6);
    if(sLine6!="") {
      if(sLine6[sLine6.length()-1]==' '||(int)sLine6[sLine6.length()-1]==13) sLine6.erase(sLine6.length()-1);  //.out 파일에서 가져오면 끝에 ASCII 코드로 13번인 안보이는게 생기는 현상 제거하기
      Out1.push_back(sLine6);
    }
  }
  infile6.close();

  char outname2[sizeof(filen2)];
  for(int i=0;i<sizeof(filen2);++i) {
    outname2[i]=filen2[i];
  }
  ifstream infile7;
  infile7.open (outname2);
  string sLine7 = "";
  while (!infile7.eof())
  {
    getline(infile7, sLine7);
    if(sLine7!="") {
      if(sLine7[sLine7.length()-1]==' '||(int)sLine7[sLine7.length()-1]==13) sLine7.erase(sLine7.length()-1);
      Out2.push_back(sLine7);
    }
  }
  infile7.close();

  char outname3[sizeof(filen3)];
  for(int i=0;i<sizeof(filen3);++i) {
    outname3[i]=filen3[i];
  }
  ifstream infile8;
  infile8.open (outname3);
  string sLine8 = "";
  while (!infile8.eof())
  {
    getline(infile8, sLine8);
    if(sLine8!="") {
      if(sLine8[sLine8.length()-1]==' '||(int)sLine8[sLine8.length()-1]==13) sLine8.erase(sLine8.length()-1);
      Out3.push_back(sLine8);
    }
  }
  infile8.close();

  char outname4[sizeof(filen4)];
  for(int i=0;i<sizeof(filen4);++i) {
    outname4[i]=filen4[i];
  }
  ifstream infile9;
  infile9.open (outname4);
  string sLine9 = "";
  while (!infile9.eof())
  {
    getline(infile9, sLine9);
    if(sLine9!="") {
      if(sLine9[sLine9.length()-1]==' '||(int)sLine9[sLine9.length()-1]==13) sLine9.erase(sLine9.length()-1);
      Out4.push_back(sLine9);
    }
  }
  infile9.close();

  char outname5[sizeof(filen5)];
  for(int i=0;i<sizeof(filen5);++i) {
    outname5[i]=filen5[i];
  }
  ifstream infile10;
  infile10.open (outname5);
  string sLine10 = "";
  while (!infile10.eof())
  {
    getline(infile10, sLine10);
    if(sLine10!="") {
      if(sLine10[sLine10.length()-1]==' '||(int)sLine10[sLine10.length()-1]==13) sLine10.erase(sLine10.length()-1);
      Out5.push_back(sLine10);
    }
  }
  infile10.close();

  //Result 1 2 3 4 5 Out 1 2 3 4 5 벡터에 입출력 파일 들 한줄씩 다 넣었음
  int total_score=0;

  int wrong1=0;
  for(int i=0;i<Result1.size();++i) {
    if(i<Out1.size()){
     if(Result1[i]!=Out1[i]) {
       cout<<"No.1, LINE "<<i<<": ["<<Result1[i]<<"] MUST BE ["<<Out1[i]<<"]"<<endl;
       wrong1=1;
     }
    }
  }
  if(wrong1==0) total_score+=2;
   
  int wrong2=0;
  for(int i=0;i<Result2.size();++i) {
    if(i<Out2.size()){
     if(Result2[i]!=Out2[i]) {
       cout<<"No.2, LINE "<<i<<": ["<<Result2[i]<<"] MUST BE ["<<Out2[i]<<"]"<<endl;
       wrong2=1;
     }
    }
  }
  if(wrong2==0) total_score+=2;

  int wrong3=0;
  for(int i=0;i<Result3.size();++i) {
    if(i<Out3.size()){
     if(Result3[i]!=Out3[i]) {
       cout<<"No.3, LINE "<<i<<": ["<<Result3[i]<<"] MUST BE ["<<Out3[i]<<"]"<<endl;
       wrong3=1;
     }
    }
  }
  if(wrong3==0) total_score+=2;

  int wrong4=0;
  for(int i=0;i<Result4.size();++i) {
    if(i<Out4.size()){
     if(Result4[i]!=Out4[i]) {
       cout<<"No.4, LINE "<<i<<": ["<<Result4[i]<<"] MUST BE ["<<Out4[i]<<"]"<<endl;
       wrong4=1;
     }
    }
  }
  if(wrong4==0) total_score+=2;

  int wrong5=0;
  for(int i=0;i<Result5.size();++i) {
    if(i<Out5.size()){
     if(Result5[i]!=Out5[i]) {
       cout<<"No.5, LINE "<<i<<": ["<<Result5[i]<<"] MUST BE ["<<Out5[i]<<"]"<<endl;
       wrong5=1;
     }
    }
  }
  if(wrong5==0) total_score+=2;  

  cout<<"SCORE: "<<total_score<<endl;

  return 0;
}

