using namespace std;
#include "bank_account.h"


unsigned int Account::ComputeExpectedBalance(unsigned int n_years_later) const {  //단리
  if(type_=="checking")
    return (balance_+(balance_*interest_rate_)*n_years_later);
  else if(type_=="saving") {
    double result=(double)balance_;
    for(int i=0;i<n_years_later;++i) {
      result+=result*interest_rate_;
    }
    return result;
  }
} 

Account::~Account() {} 
  



SavingAccount::~SavingAccount() {} 



