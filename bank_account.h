#ifndef _BANK_ACCOUNT_H_
#define _BANK_ACCOUNT_H_

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <iomanip>
#include <fstream>
#include <sstream>

class Account {  
 public: 
  Account() {}
  Account(const string&  type, const string& name, unsigned int balance, double interest_rate) {
    type_=type;
    name_=name;
    balance_=balance;
    interest_rate_=interest_rate;
  }
  virtual ~Account(); 

 
  virtual unsigned int ComputeExpectedBalance(unsigned int n_years_later) const; 
 
  virtual const string type() const { return type_; } 
  const unsigned int& balance() const { return balance_; } 
  const string& name() const { return name_; } 
  const double& interest_rate() const { return interest_rate_; } 
 
 
 protected: 
  string name_; 
  unsigned int balance_; 
  double interest_rate_;  //  기본  계좌는  단리로  계산.
  string type_;
}; 

class SavingAccount : public Account { 
 public: 
  SavingAccount() {}
  SavingAccount(const string&  type, const string& name, unsigned int balance, double interest_rate) {
    type_=type;
    name_=name;
    balance_=balance;
    interest_rate_=interest_rate;
  } 

  virtual ~SavingAccount(); 

  virtual const string type() const { return type_; }
  //  이  타입의  계좌는  복리로  계산. 
 
};   
 
 

#endif /* bank_account.h */
