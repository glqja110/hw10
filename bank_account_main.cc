using namespace std;
#include "bank_account.h"

int main() {
  vector<Account> list;
  vector<Account>::iterator it;
  string cmd;
  while (cmd != "quit") {  
    cin >> cmd;
    /*add < 이름> < 종류> < 금액> < 이자율>
     checking :  해당  고객의  이자는  단리로  계산 
     saving :  해당  고객의  이자는  복리로  계산 */
    if (cmd == "add") {  
      string name;
      string kind;
      unsigned int money;
      double interest;
      cin>>name>>kind;
      if(kind=="checking") {
        cin>>money>>interest;
        Account acc(kind, name, money, interest);
        list.push_back(acc);
      }
      else if(kind=="saving") {
        cin>>money>>interest;
        SavingAccount sac(kind, name, money, interest);
        list.push_back(sac);
      }
    } else if (cmd == "delete") {  //delete < 이름>
      string name2;
      cin>>name2;
      for(it=list.begin();it!=list.end();++it) {
        if(it->name()==name2) break;
      }
      list.erase(it);    
    } else if (cmd == "show") {   
      for(it=list.begin();it!=list.end();++it) {
        cout<<it->name()<<"\t"<<it->type()<<"\t"<<it->balance()<<"\t"<<it->interest_rate()<<endl;
      }
    } else if (cmd == "after") {   //after <n_years>
      unsigned int year;
      cin>>year;
      for(it=list.begin();it!=list.end();++it) {
        cout<<it->name()<<"\t"<<it->type()<<"\t"<<it->ComputeExpectedBalance(year)<<"\t"<<it->interest_rate()<<endl;
      }
    }
     
  }
  return 0;
}
