using namespace std;
#include "grader.h"

// 성적을 Pass / Fail 로 구분하여 출력해주는 클래스.
// 성적이 pass_score보다 같거나 높으면 "P", 아니면 "F"를 리턴.

string SubjectPassFail::GetGrade(int score) const {
  if(score>=pass_score_) return "P";
  else return "F";	
}


// 성적을 A, B, C, D, F 로 구분하여 출력해주는 클래스.
// 성적이 속하는 구간에 따라
// 100 >= "A" >= cutA > "B" >= cutB > "C" >= cutC > "D" >= cutD > "F".

string SubjectGrade::GetGrade(int score) const {
  if(kind_ == "G") {
    if(100>=score&&score>=cutA_) return "A";
    else if(cutA_>score&&score>=cutB_) return "B";
    else if(cutB_>score&&score>=cutC_) return "C";
    else if(cutC_>score&&score>=cutD_) return "D";
    else if(cutD_>score) return "F";
  } else if(kind_ == "G+") {
    if(100>=score&&score>=(100+cutA_)/2) return "A+";
    else if((100+cutA_)/2>score&&score>=cutA_) return "A";
    else if(cutA_>score&&score>=(cutA_+cutB_)/2) return "B+";
    else if((cutA_+cutB_)/2>score&&score>=cutB_) return "B";
    else if(cutB_>score&&score>=(cutB_+cutC_)/2) return "C+";
    else if((cutB_+cutC_)/2>score&&score>=cutC_) return "C";
    else if(cutC_>score&&score>=(cutC_+cutD_)/2) return "D+";
    else if((cutC_+cutD_)/2>score&&score>=cutD_) return "D";
    else if(cutD_>score) return "F";
  }
}
