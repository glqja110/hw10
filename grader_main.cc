using namespace std;
#include "grader.h"

inline bool CompareStudent(const pair<string, double>& a,const pair<string, double>& b) {
  return (a.second>b.second);
}

double GetNumberGrade(const string& str) {
  if (str == "A+") return 4.5;
  if (str == "A" || str == "P") return 4.0;
  if (str == "B+") return 3.5;
  if (str == "B") return 3.0;
  if (str == "C+") return 2.5;
  if (str == "C") return 2.0;
  if (str == "D+") return 1.5;
  if (str == "D") return 1.0;
  return 0.0;
}

int main() {
  string cmd;
  vector<pair<string, double> > student_grades;
  int Subject_count=0;
  vector<int> scores;
  vector<Subject*> SubjectClass;
  vector<string> Grades;

  while (cmd != "quit") {
    cin >> cmd;
    
    if(cmd == "subject") {  //subject [과목이름] [credit] [종류]
      string Subject_;
      int credit_;
      string kind_;
      int CutA, CutB, CutC, CutD, CutP;
      cin>>Subject_>>credit_>>kind_;              //과목이름 credit 종류를 입력
     
      if(kind_ == "G+"||kind_ == "G") {
        cin>>CutA>>CutB>>CutC>>CutD; 
        SubjectClass.push_back(new SubjectGrade(Subject_, credit_, CutA, CutB, CutC, CutD, kind_));
      }  else if(kind_ == "PF") {
        cin>>CutP;
        SubjectClass.push_back(new SubjectPassFail(Subject_, credit_, CutP));
      }
      Subject_count++;
    }

    else if (cmd == "eval") {
      string name;
      
      cin>>name;
      for(int i=0;i<Subject_count;++i) {
        int score_;
        cin>>score_;
        scores.push_back(score_);
      }
      cout<<"\t"<< name << "\t";
      for(int i=0;i<Subject_count;++i) {
        string grade_= SubjectClass[i]->GetGrade(scores[i]);
        if(i!=Subject_count-1) cout<<grade_<< "\t";
        else cout<<grade_<<endl;
        Grades.push_back(grade_);
      }
      int total_credit=0;
      for(int i=0; i<Subject_count;++i) {
        total_credit+=SubjectClass[i]->credit();
      } 

      double average_grade=0.0;
      for(int i=0; i<Subject_count;++i) {
        average_grade+=GetNumberGrade(Grades[i]) * SubjectClass[i]->credit();
      }
      average_grade=average_grade/total_credit;

      student_grades.push_back(make_pair(name, average_grade));
      scores.clear();
      Grades.clear();
    }
  }
  sort(student_grades.begin(), student_grades.end(), CompareStudent);
  for (int i = 0; i < student_grades.size(); ++i) {
    cout << student_grades[i].first << " ";
    cout << setprecision(3)<<student_grades[i].second << endl;
  }
  return 0;
}
